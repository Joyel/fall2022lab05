//Joyel Selvakumar, 2111271

/**
 * Class Cone contains empty methods as bare
 * minimum for the production class and any unit tests.
 * Now has actual production code in most recent build.
 * 
 * @author Joyel Selvakumar
 * @version 9/29/2022
 */

package javalab5;

public class Cone implements Shape3d {
    private double radius;
    private double height;

    
    /**
     * Creates a Cone object with height and radius values
     * 
     * @throws IllegalArgumentException for radius inputs 0 and less
     */
    public Cone(double newRadius, double newHeight){
        if (newRadius <= 0 || newHeight <= 0){
            throw new IllegalArgumentException("Radius and height should not be 0 or negative values.");
        }

        this.radius = newRadius;
        this.height = newHeight;
    }

    /**
     * Returns the radius field of the Cone object
     * 
     * @return The value of the Cone radius
     */
    public double getRadius(){
        return this.radius;
    }

    /**
     * Returns the height field of the Cone object
     * 
     * @return The value of the Cone height
     */
    public double getHeight(){
        return this.height;
    }

    /**
     * Returns the volume of the created Cone object
     * 
     * @return The value of the Cone volume
     */
    @Override
     public double getVolume(){
        return Math.PI * Math.pow(this.radius, 2) * (this.height /3);
    }

    /**
     * Returns the surface area of the created Cone object
     * 
     * @return The value of the Cone surface area
     */
    @Override
     public double getSurfaceArea(){
        return Math.PI * this.radius * (this.radius + Math.sqrt(Math.pow(this.radius, 2) + Math.pow(this.height, 2)));
    }
}
