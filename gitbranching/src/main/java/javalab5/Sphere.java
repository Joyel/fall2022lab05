//Jimmy Xu, 2138599
package javalab5;
/**
 * Sphere is a class that represents a physical Shere but digitally
 * 
 * @author Jimmy Xu
 * @version 9/29/2022
 */
public class Sphere implements Shape3d{
    private double radius;
    /**
     * contructor for Sphere
     * @throws IllegalArgumentException for a radius below 0
     */
    public Sphere(double r){
        if(r<=0){
            throw new IllegalArgumentException("radius can't be equal or less than 0");
        }
        this.radius=r;
    }

    /**
     * gets radius field 
     * @return radius field value
     */
    public double getRadius(){
        return this.radius;
    }

    /**
     * computes volume of the Sphere
     * 
     * @return the volumne of the Sphere
     */
    @Override
    public double getVolume(){
        return Math.pow(this.radius,3)*Math.PI*(4/3.0);
    }

    /**
     * computes the surface area of the Sphere
     * 
     * @return the surface area of the Sphere
     */
    @Override
    public double getSurfaceArea(){
        return Math.pow(this.radius,2)*Math.PI*4;
    }
}
