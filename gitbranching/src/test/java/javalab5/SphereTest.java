//Joyel Selvakumar, 2111271

package javalab5;

import static org.junit.Assert.*;

import org.junit.Test;

public class SphereTest {
    private final double TOLERANCE = 0.0000001;

    @Test
    public void testGetSphereConstructor(){
        try{
            Sphere testSphere = new Sphere(-2);

            fail("If you see this, the data validation on 0 or negative radius failed.");
        }

        catch(IllegalArgumentException e){
            System.out.println("Invalid sphere input (radius) caught. Data validation successful.");
        }
    }
    
    @Test
    public void testSphereGetRadius(){
        Sphere testSphere = new Sphere(2.227);

        assertEquals(2.227, testSphere.getRadius(), TOLERANCE);
    }

    @Test
    public void testSphereGetVolume(){
        Sphere testSphere = new Sphere(9.2113);

        assertEquals((4/3.0) * Math.PI * Math.pow(9.2113, 3), testSphere.getVolume(), TOLERANCE);
    }

    @Test
    public void testSphereGetSurfaceArea(){
        Sphere testSphere = new Sphere(7.823);

        assertEquals(4 * Math.PI * Math.pow(7.823, 2), testSphere.getSurfaceArea(), TOLERANCE);
    }
}
