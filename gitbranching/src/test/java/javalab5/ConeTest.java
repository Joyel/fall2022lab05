//Jimmy Xu, 2138599
package javalab5;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class ConeTest 
{
    /**
     * Rigorous Test :-)
     */
    private static final double TOLERANCE=0.0001;
    @Test
    public void constructorTest()
    {
        try{
            Cone c2 = new Cone(0,567.89);
            fail("Expecting exception for radius of 0 but it didnt throw");
        }
        catch(IllegalArgumentException e){

        }
        try{
            Cone c3 = new Cone(123.312,-192.002);
            fail("Expecting exception for negative height but it didnt throw");
        }
        catch(IllegalArgumentException e){

        }
    }

    @Test
    public void gettersTest(){
        Cone c1 = new Cone(5.43,0.11);
        assertEquals(5.43,c1.getRadius(),TOLERANCE);
        assertEquals(0.11,c1.getHeight(),TOLERANCE);
    }

    @Test
    public void volumeTest(){
        Cone c = new Cone(5.21,7.78);
        assertEquals(221.1483,c.getVolume(),TOLERANCE);
    }

    @Test
    public void surfaceAreaTest(){
        Cone c = new Cone(0.43,9.03);
        assertEquals(12.7931,c.getSurfaceArea(),TOLERANCE);
    }
}
